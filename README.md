# What is this?

First project to use opengl. The original code came from this site:
http://www.swiftless.com/tutorials/opengl/rotation.html

# What do you need?

you need to install:
 - git
 - cmake
 - make
 - OpenGL
 - This packets (sudo apt-get install libxmu-dev libxi-dev freeglut3 freeglut3-dev libglew-dev)
 - Eclipse (Optional, the eclipse environment is available, but it is not mandatory).
 - valgrind (Optional, just to check memory leak)

# How do I build?

The project has two targets (`debug` and `debug-test`) and you can build the project in two different ways:
 - Entering in the folder: `/cplusplus-opengl-test/scripts` and executing `./build.sh <target>`
 - Open the Eclipse program, import the project and run the respective target.

# Where are the results ?

All the output files, including the executable file, are inserted in the respective builder folder of the project:
 - debug:
	`/cplusplus-opengl-test/build/debug/`
 - debug-test:
	`/cplusplus-opengl-test/build/debug-test/`

# How to run the programs?

You should enter in the respective output folder and execute the binary file.

	`./debug`
	`./debug-test`
